Router.route('/', function () {
  this.render('home');
  if(localStorage.getItem("markdown").length > 1){
    this.redirect('/view');
    loadThemeStylesheet();
  }
  if(!localStorage.getItem("returnVisitor")){
    this.redirect('/about');
  }
});

Router.route('/about', function () {
  this.render('about');
  localStorage.setItem("returnVisitor", true);  
  if(!localStorage.getItem("aboutText")){
    setTimeout(function(){
      location.reload();
    },2000);
  }
});


Router.route('/view', function () {
  console.log('view route');
  if(localStorage.getItem("markdown").length > 1){
    this.render('output', {
      // data: function () { return Items.findOne({_id: this.params._id}); }
    });
    Template.output.rendered = function(){
      console.log("bore if");
      if(document.getElementById("output-container").innerHTML.length < 100){
        console.log("after if");
        
        setTimeout(function(){
          window.location.reload()
        },5000)
      }
      checkDropcap();
    }
    
  }else{
    this.redirect('/');
  }
});

Router.route('/edit', function () {
  Session.set("markdown", "");
  this.render('Home', {
    // data: function () { return Items.findOne({_id: this.params._id}); }
  });
});

Router.route('/gist/:_id',  function () {
  var params = this.params; // { _id: "5" }
  var id = params._id; // "5"
  console.log("Route run for gist Id: "+id);
  loadFromGistUrl(id);  
});

Router.route('pdf', function(){
  this.render('pdf')
  
  var pdf = new jsPDF('p','pt','a4');
  pdf.addHTML(document.body,function() {
      var string = pdf.output('datauristring');
      string.save("test.pdf");
  });

  // doc.save('Test.pdf');
  // console.log("iISSSS")
  // if (_.isFunction(window.callPhantom)){
  //  Meteor.setTimeout(function() {
  //    window.callPhantom('takeShot');
  //  }, 500);
  // }
});

function getMarkdown(){
  // var data = Meteor.call('fetchMarkdown', function(err, data) {
  //   if (err){
  //     console.log(err);
  //   };
  //   Session.set("markdown", data);
  // });
  Session.set("markdown", localStorage.getItem("markdown"));
}

getMarkdown();


function clearMarkdown(){
  // var data = Meteor.call('emptyDB', function(err, data) {
  //   if (err){
  //     console.log(err);
  //   }
  //   Session.set("markdown", "");
  // });
  console.log("clearMarkdown()")
  localStorage.setItem("markdown", "");
  localStorage.setItem("gistContent", "");
  Session.set("markdown", "");
}

function updateMarkdown(markdown){
  localStorage.setItem("markdown", markdown);
  Session.set("markdown", markdown);
}

function loadThemeStylesheet(){
  if(localStorage.getItem("preset")){
    var preset = localStorage.getItem("preset");
  }else{
    var preset = "theme1";
  }
  
  if(Session.get("preset-name")){
    var presetName = localStorage.getItem("preset-name");
  }else{
    var presetName = "Basic";
  }
  
  sheets = document.getElementsByClassName("theme-stylesheet");
  for(var i = 0; i < sheets.length; i++){
    sheets[i].parentNode.removeChild(sheets[i]);
  }
        
  var cb = function() {
    var l = document.createElement('link'); l.rel = 'stylesheet';
    l.href = '/themes/'+preset+'.css';
    l.id = preset+"-stylesheet";
    l.className = "theme-stylesheet";
    var h = document.getElementsByTagName('head')[0];
    h.parentNode.insertBefore(l, h);
  };
  var raf = requestAnimationFrame || mozRequestAnimationFrame ||
      webkitRequestAnimationFrame || msRequestAnimationFrame;
  if (raf) raf(cb);
  else window.addEventListener('load', cb);
}


// var GitHub = require("github");
// 
// function getGistContent(gistId){
//     var github = new GitHub({
//         version: "3.0.0", // required
//         timeout: 5000     // optional
//     });
//     
//     var resp = github.gists.get({id:gistId});
//     // console.log(resp);
//     // return resp;
// 
//     for (var property in resp.files) {
//         if (resp.files.hasOwnProperty (property) ) {
//           console.log(resp.files[property].content);
//           return resp.files[property].content;
//         }
//     }
// }

function getJSONP(url, success) {
  var ud = '_' + +new Date,
      script = document.createElement('script'),
      head = document.getElementsByTagName('head')[0] 
             || document.documentElement;
  window[ud] = function(data) {
      head.removeChild(script);
      success && success(data);
  };
  script.src = url.replace('callback=?', 'callback=' + ud);
  head.appendChild(script);
}

function getRawUrl(data){
  var gist = data.data;
  var files = gist.files;
  for (var property in files) {
    if (files.hasOwnProperty (property) ) {
     var rawUrl = files[property].raw_url;
     return rawUrl;
    }     
  }
}

function getGistContent(data){
  var gist = data.data;
  var files = gist.files;
  for (var property in files) {
    if (files.hasOwnProperty (property) ) {
     var content = files[property].content;
     return content;  
    }     
  }
}

function getMarkdownFromGist(gistId){
  console.log("getMarkdownFromGist(gistId)");
  getJSONP('http://api.github.com/gists/'+gistId+'?callback=?', function(data){
    console.log(data);
    var gistContent = getGistContent(data);
    console.log("Content in jsonP of getMAkrodnwfrom Gist, via getGistContent"+gistContent);
    if(data.data['documentation_url'] == "https://developer.github.com/v3/#rate-limiting") {
      var gistContent = "<br>Sorry, we’ve exceeded GitHub’s rate limit for the time being. Please wait an hour or so and try again.";
    }    
  	Session.set("rawUrl", getRawUrl(data));
    Session.set("gistContent", gistContent);
    localStorage.setItem("gistContent", gistContent);
    updateMarkdown(gistContent);
  });
  console.log("This is session get in getmdfromgist: "+Session.get("markdown"));
  return Session.get("markdown");
};

function getAboutTextFromGist(){
  var gistId = "fb83089148ea52c512ad";
  getJSONP('http://api.github.com/gists/'+gistId+'?callback=?', function(data){
    var gistContent = getGistContent(data);
    console.log(data.data['documentation_url']);
    if(data.data['documentation_url'] == "https://developer.github.com/v3/#rate-limiting") {
      var gistContent = "<br>Sorry, we’ve exceeded GitHub’s rate limit for the time being. Please wait an hour or so and try again.";
    }    
    localStorage.setItem("aboutText", gistContent);
  });
  localStorage.setItem("returnVisitor", true);
  return localStorage.getItem("aboutText");
}

function loadFromGistUrl(gistId){
  console.log("loadFromGistUrl(gistId)");
  clearMarkdown();
  getMarkdownFromGist(gistId);
  setTimeout(function(){
    Router.go('/view');
  },1000)
}

function loadFromGist(gistId){
  clearMarkdown();
  getMarkdownFromGist(gistId);
  var gistContent = Session.get("markdown");
  document.getElementById("input-text").value = gistContent;
  document.getElementById("input-text").style.border = "1px solid orange";
  setTimeout(function(){
    window.location.reload();
  },2000);
}

function checkDropcap(){
  var firstP = document.getElementsByTagName("p")[0];
  var lvalue = firstP.innerHTML;
  var larr = lvalue.split(' ');
  var firstParagraphLength = 0;
  // For each iterates over the index of arrays
  for(var i in larr) { 
    firstParagraphLength += larr[ i ].length // Acummulate the length of all the strings
  };
  if (firstParagraphLength < 100) {
    firstP.setAttribute('class','no-dropcap');
  }
}

Template.output.helpers({
  outputContent: function(){
    loadThemeStylesheet();
    return Session.get("markdown");
  },
  preset: function(){
    return localStorage.getItem("preset")
  },
  presetName: function(){
    return Session.get("preset-name")
  }
});

Template.input.helpers({
  noMarkdown: function(){
    if(Session.get("markdown")){
      return false;
    }else{
      return true;
    }
  }
});

Template.about.helpers({
  aboutText: function(){
    return getAboutTextFromGist();
  }
})

Template.input.events({
  "click #submit-button": function(){
    var e = document.getElementById("preset-select");
    var preset = e.options[e.selectedIndex].value;
    var presetName = e.options[e.selectedIndex].text;
    
    localStorage.setItem("preset", preset);
    localStorage.setItem("preset-name", presetName);
  
    Session.set("preset", preset);
    Session.set("preset-name", presetName);
    
    Router.go('/view');
  },
  
  // "click #get-gist": function(){
  //   console.log("#get-gist clicked");
  //   var e = document.getElementById("input-gist");
  //   var gistId = e.value;
  //   loadFromGist(gistId);
  // }
})

Template.inputText.helpers({
  currentMarkdown: function(){
    return localStorage.getItem("markdown");
  }
});

Template.output.events({
  "click .clear-session": function(){
    Session.set("markdown", "");
    window.location = "/edit";
  },
  
  "click .clear-localstorage": function(){
    localStorage.setItem("markdown", "");
    window.location = "/";
    
  }
});

Template.options.helpers({
  preset: function(){
    return localStorage.getItem("preset")
  },
  presets: function(){
    var presets = [
        {"preset": "theme1", "presetName": "Basic"},
        {"preset": "theme2", "presetName": "Academic"},
        {"preset": "theme3", "presetName": "Hipster"}
    ];
    for(item in presets){
       if(presets[item].preset == localStorage.getItem("preset")){
         presets[item].active = "selected"
       }
    }
    return presets;
  },
});

Template.options.events({
  "click .clear-markdown": function(){
    clearMarkdown();
    window.location.reload();
    Router.go('/'); 
  },
  "click #typeset-button": function(){
    
  }
})

// Template.header.events({
//   
// })

Template.inputText.events({
  "submit form": function(event, template){
    event.preventDefault();
    var inputText = document.getElementById("input-text").value;
    updateMarkdown(inputText);
    getMarkdown();
  }
});


Template.inputGist.events({
  "submit form": function(event, template){
    event.preventDefault();
    console.log("inputGist submit function");
    var inputGist = document.getElementById("input-gist");
    var gistId = inputGist.value;
    console.log("GISTID: "+gistId);
    loadFromGist(gistId)
    inputGist.style.border = "1px solid green";
    document.getElementById('input-text').focus();
  }
})